const { parseXML, validate } = require('./converter/xml2json');
const format = require('date-fns/format');
const generateInvoice = require('./generateInvoice');

function formatDate(dateString) {
    return format(new Date(dateString), 'yyyy-MM-dd');
}

function processData(xmlData, {
    firstName,
    secondName,
    lastName,
    address,
    bulstat,
    companyName,
    contractDate,
    contractNumber,
    documentDate,
    documentNumber,
    eGN
}) {
    if(validate(xmlData)) {
        const jsonObj = parseXML(xmlData);
        const {
            monthly_notification_num,
            practice_code,
            date_from,
            date_to,
            inv_type_code,
            nhif_type_code,
            inv_summ,
            Monthly_Notification_Details
        } = jsonObj.Monthly_Notification;
    
        const generatedXML = generateInvoice({
            documentType: inv_type_code,
            documentNumber: documentNumber,
            documentMonthNumber: monthly_notification_num,
            documentDate: formatDate(documentDate),
            companyName: companyName,
            graoNumber: eGN,
            firstName: firstName,
            secondName: secondName,
            lastName: lastName,
            addressByContract: address,
            issuerBulstat: bulstat,
            contractNumber: contractNumber,
            contractDate: formatDate(contractDate),
            practiceCode: practice_code,
            activityTypeCode: nhif_type_code,
            dateFrom: date_from,
            dateTo: date_to,
            monthlyNotificationDetails: Monthly_Notification_Details,
            invoiceSum: inv_summ
        });
    
        return generatedXML;
    } else {
        throw new Error("Invalid XML document (Kappa Error)");
    }
}

module.exports = processData;