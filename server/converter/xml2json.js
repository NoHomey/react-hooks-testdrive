const parser = require('fast-xml-parser');

const options = {
    ignoreNameSpace : false,
    allowBooleanAttributes : true,
    parseNodeValue : true,
    parseAttributeValue : false,
    trimValues: true,
    localeRange: "",
    parseTrueNumberOnly: true
};

function parseXML(xmlText) {
    return parser.parse(xmlText, options);
}

function validate(xmlText) {
    return parser.validate(xmlText);
}

module.exports = {
    parseXML,
    validate
};