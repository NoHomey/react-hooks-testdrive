const Parser = require("fast-xml-parser").j2xParser;

const options = {
    attributeNamePrefix : "@_",
    attrNodeName: "@",
    textNodeName : "#text",
    ignoreAttributes : true,
    cdataTagName: "__cdata",
    cdataPositionChar: "\\c",
    format: true,
    indentBy: "  ",
    supressEmptyNode: false
};

const jsonParser = new Parser(options);

function convert(jsonObj) {
    return jsonParser.parse(jsonObj);
}

module.exports = convert;