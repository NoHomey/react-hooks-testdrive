const path = require('path');
const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

const processData = require('./processData');
const sendXML = require('./sendXML');

const app = express();
const port = process.env.PORT || 5000;

const static = path.join(__dirname, '../build');

console.log(static);

app.use(express.static(static));
app.use(bodyParser.json());
app.use(fileUpload());

app.post('/api', (req, res) => {
    const data = req.body;
    const xmlData = req.files.file.data.toString();
    const result = processData(xmlData, data);
    sendXML(result, res);
});

app.listen(port, () => console.log(`Listening on port: ${port}!`));