const tmp = require('tmp');
const fs = require('fs');

function tempFile() {
    return new Promise((resolve, reject) => {
        tmp.file((err, path, fd, clean) => {
            if(err) {
                reject(err);
            } else {
                resolve({ path, fd, clean });
            }
        });
    });
}

function writeToFD(fd, buff) {
    return new Promise((resolve, reject) => {
        fs.write(fd, buff, (err) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function closeFD(fd) {
    return new Promise((resolve, reject) => {
        fs.close(fd, (err) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function sendFile(res, path) {
    return new Promise((resolve, reject) => {
        res.download(path, 'result.xml', (err) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function sendError(res, reason, clean) {
    if(clean) {
        clean();
    }
    res.status(500).json({ reason });
}

async function sendXML(xml, res) {
    try {
        const { path, fd, clean } = await tempFile();
        const buff = Buffer.from(xml, 'utf8');
        try {
            await writeToFD(fd, buff);
            try {
                await closeFD(fd);
                try {
                    await sendFile(res, path);
                    clean();
                    console.log('success');
                } catch(err) {
                    console.log(err);
                    console.log('error');
                }
            } catch(err) {
                console.log(err);
                sendError(res, 'close file', clean);
            }
        } catch(err) {
            console.log(err);
            sendError(res, 'write file', clean);
        }
    } catch(err) {
        console.log(err);
        sendError(res, 'tmp file');
    }
}

module.exports = sendXML;