const convertJSON2XML = require('./converter/json2xml');

function generateInvoice({
    documentType,
    documentNumber,
    documentMonthNumber,
    documentDate,
    companyName,
    graoNumber,
    firstName,
    secondName,
    lastName,
    addressByContract,
    issuerBulstat,
    contractNumber,
    contractDate,
    practiceCode,
    activityTypeCode,
    dateFrom,
    dateTo,
    monthlyNotificationDetails,
    invoiceSum
}) {
    const jsonObj = {
        ELECTRONIC_INVOICE: {
            fin_document_type_code: documentType,
            fin_document_no: documentNumber,
            fin_document_month_no: documentMonthNumber,
            fin_document_date: documentDate,
            Invoice_Recipient: {
                recipient_cod: 22,
                recipient_name: "СЗОК",
                recipient_address: "гр София ул Енос 10 вх Б",
                recipient_bulstat: 1218582201412
            },
            Invoice_Issuer: {
                issuer_type: 1,
                self_insured: "Y",
                self_insured_declaration: "Декларация по чл.43,ал.5 от ЗДДФЛ, че съм самоосигуряващо се лице по смисъла на КСО",
                company_name: companyName,
                Person_Info: {
                    Identifier: {
                        grao_no: graoNumber
                    },
                    first_name: firstName,
                    second_name: secondName,
                    last_name: lastName
                },
                address_by_contract: addressByContract,
                registration_by_VAT: 0,
                grounds_for_not_charging_VAT: "Чл.113,ал.9 от ЗДДС",
                issuer_bulstat: issuerBulstat,
                contract_no: contractNumber,
                contract_date: contractDate,
                rhi_nhif_no: practiceCode,
            },
            health_insurance_fund_type_code: "NZOK",
            activity_type_code: activityTypeCode,
            date_from: dateFrom,
            date_to: dateTo,
            Business_operation: monthlyNotificationDetails.map(monthlyNotificationDetail => ({ ...monthlyNotificationDetail, measure_code: "BR" })),
            Aggregated_amounts: {
                payment_type: "B",
                total_amount: invoiceSum,
                payment_amount: invoiceSum,
                original: "Y",
                tax_event_date: dateTo
            }
        }
    };

    return convertJSON2XML(jsonObj);
}

module.exports = generateInvoice;