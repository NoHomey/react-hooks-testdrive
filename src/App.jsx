import React, { Fragment } from 'react';
import Form from './view/Form';
import Result from './view/Result';
import useLogic from './logic/useLogic';

export default function App() {
    const [{
        firstName,
        secondName,
        lastName,
        address,
        bulstat,
        companyName,
        contractDate,
        contractNumber,
        documentDate,
        documentNumber,
        eGN,
        file,
        result
    }, {
        setFirstName,
        setSecondName,
        setLastName,
        setAddress,
        setBulstat,
        setCompanyName,
        setContractDate,
        setContractNumber,
        setDocumentDate,
        setDocumentNumber,
        setEGN,
        setFile
    }, {
        handleSubmit,
        clear
    }] = useLogic();

    return (
        <Fragment>
            <Form 
                firstName={firstName}
                setFirstName={setFirstName}
                secondName={secondName}
                setSecondName={setSecondName}
                lastName={lastName}
                setLastName={setLastName}
                address={address}
                setAddress={setAddress}
                bulstat={bulstat}
                setBulstat={setBulstat}
                companyName={companyName}
                setCompanyName={setCompanyName}
                contractDate={contractDate}
                setContractDate={setContractDate}
                contractNumber={contractNumber}
                setContractNumber={setContractNumber}
                documentDate={documentDate}
                setDocumentDate={setDocumentDate}
                documentNumber={documentNumber}
                setDocumentNumber={setDocumentNumber}
                eGN={eGN}
                setEGN={setEGN}
                file={file}
                setFile={setFile}
                onSubmit={handleSubmit}
            />
            <Result result={result} onOk={clear} />
        </Fragment>
    );
};
