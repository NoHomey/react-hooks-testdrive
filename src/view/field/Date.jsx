import React, { useCallback } from 'react';
import { DatePicker } from "@material-ui/pickers";
import InputField from './InputField';

export default function Date({
    label,
    value,
    name,
    onChange
}) {
    const handelChange = useCallback(
        date => onChange(date),
        [onChange]
    );
    return (
        <DatePicker
            name={name}
            value={value}
            label={label}
            format="dd.MM.yyyy"
            variant="dialog"
            disableFuture
            TextFieldComponent={InputField}
            onChange={handelChange} />
    );
}