import React from 'react';
import Field from './Field';

export default function Bulstat({
    bulstat,
    setBulstat
}) {
    return (
        <Field
            name="bulstat"
            label="Булстат"
            value={bulstat}
            onChange={setBulstat} />
    );
};
