import React from 'react';
import Field from './Field';

export default function SecondName({
    secondName,
    setSecondName
}) {
    return (
        <Field
            name="secondName"
            label="Презиме"
            value={secondName}
            onChange={setSecondName} />
    );
};