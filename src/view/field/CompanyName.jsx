import React from 'react';
import Field from './Field';

export default function CompanyName({
    companyName,
    setCompanyName
}) {
    return (
        <Field
            name="companyName"
            label="Име на фирма"
            value={companyName}
            onChange={setCompanyName} />
    );
};