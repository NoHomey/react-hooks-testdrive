import React, { useCallback } from 'react';
import InputField from './InputField';

export default function Field({
    name,
    type,
    label,
    value,
    onChange
}) {
    const handelChange = useCallback(
        event => onChange(event.target.value),
        [onChange]
    );
    return (
        <InputField
            name={name}
            type={type}
            label={label}
            value={value}
            onChange={handelChange} />
    );
};