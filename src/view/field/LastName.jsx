import React from 'react';
import Field from './Field';

export default function LastName({
    lastName,
    setLastName
}) {
    return (
        <Field
            name="lastName"
            label="Фамилия"
            value={lastName}
            onChange={setLastName} />
    );
};