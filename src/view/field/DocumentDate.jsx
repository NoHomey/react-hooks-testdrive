import React from 'react';
import Date from './Date';

export default function DocumentDate({
    documentDate,
    setDocumentDate
}) {
    return (
        <Date
            name="documentDate"
            label="Дата на фактура"
            value={documentDate}
            onChange={setDocumentDate} />
    );
};
