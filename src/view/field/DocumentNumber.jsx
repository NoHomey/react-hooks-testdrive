import React from 'react';
import Field from './Field';

export default function DocumentNumber({
    documentNumber,
    setDocumentNumber
}) {
    return (
        <Field
            name="documentNumber"
            label="Номер на фактурата"
            value={documentNumber}
            onChange={setDocumentNumber} />
    );
};
