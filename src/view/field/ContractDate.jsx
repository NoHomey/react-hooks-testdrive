import React from 'react';
import Date from './Date';

export default function ContractDate({
    contractDate,
    setContractDate
}) {
    return (
        <Date
            name="contractDate"
            label="Дата на договора"
            value={contractDate}
            onChange={setContractDate} />
    );
};
