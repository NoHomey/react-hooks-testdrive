import React from 'react';
import Field from './Field';

export default function EGN({
    eGN,
    setEGN
}) {
    return (
        <Field
            name="eGN"
            label="ЕГН"
            value={eGN}
            onChange={setEGN} />
    );
};
