import React from 'react';
import Field from './Field';

export default function FirstName({
    firstName,
    setFirstName
}) {
    return (
        <Field
            name="firstName"
            label="Име"
            value={firstName}
            onChange={setFirstName} />
    );
};