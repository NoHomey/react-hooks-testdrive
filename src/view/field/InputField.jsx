import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function InputField({
    value,
    ...rest
}) {
    const error = value === '';
    return (
        <TextField
            {...rest}
            value={value}
            variant="outlined"
            fullWidth
            error={error}
            helperText={error && 'Това поле е задължително'} />
    );
};