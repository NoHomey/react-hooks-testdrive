import React from 'react';
import Field from './Field';

export default function ContractNumber({
    contractNumber,
    setContractNumber
}) {
    return (
        <Field
            name="contractNumber"
            label="Номер на договора"
            value={contractNumber}
            onChange={setContractNumber} />
    );
};
