import React from 'react';
import Field from './Field';

export default function Address({
    address,
    setAddress
}) {
    return (
        <Field
            name="address"
            label="Адрес на лечебното заведение"
            value={address}
            onChange={setAddress} />
    );
};
