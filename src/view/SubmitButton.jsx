import React from 'react';
import Button from '@material-ui/core/Button';

export default function SubmitButton({
    className,
    onSubmit
}) {
    return (
        <Button
            type="button"
            fullWidth
            variant="outlined"
            color="primary"
            className={className}
            onClick={onSubmit} >
            Изпрати
          </Button>
    );
};