import React from 'react';
import FileSelect from './FileSelect';
import SelectedFile from './SelectedFile';
import { styled } from '@material-ui/styles';

const FileSelectWithMarginRight = styled(FileSelect)({ marginRight: 16 });

export default function FileSelector({
    file,
    onFileChange
}) {
    const isFileSelected = file && file.name !== '';
    const fileName = isFileSelected ? file.name : '';
    return (
        <span>
            <FileSelectWithMarginRight
                onFileChange={onFileChange}
                selectedFile={isFileSelected } />
            <SelectedFile
                fileName={fileName}
                error={!isFileSelected } />
        </span>
    );
};