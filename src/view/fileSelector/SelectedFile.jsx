import React from 'react';
import Chip from '@material-ui/core/Chip';
import { styled } from '@material-ui/styles';

const StyledChip = styled(Chip)({ fontSize: 18 });

export default function SelectedFile({ fileName }) {
    const error = fileName === '';
    const text = error ? 'Не е избран файл' : fileName;
    const color = error ? 'secondary' : 'primary';
    return (
        <StyledChip
            size="medium"
            variant="outlined"
            color={color}
            label={text} />
    );
};