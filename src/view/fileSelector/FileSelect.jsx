import React, { useCallback } from 'react';
import Button from '@material-ui/core/Button';
import { styled } from '@material-ui/styles';

const id = "file-select";

function FileInput({
    className,
    onFileChange
}) {
    const handleInputChange = useCallback(
        event => onFileChange(event.target.files[0]),
        [onFileChange]
    );
    return (
        <input
            accept=".xml"
            className={className}
            id={id}
            type="file"
            onChange={handleInputChange} />
    );
}

const Input = styled(FileInput)({ display: 'none' });

function FileSelectButton({ selectedFile }) {
    return (
        <Button
            component='span'
            variant='outlined'
            color='primary' >
            {selectedFile ? 'Променете избрания файл' : 'Изберете файл'}
        </Button>
    );
}

function FileSelect({
    className,
    onFileChange,
    selectedFile
}) {
    return (
        <span className={className}>
            <Input onFileChange={onFileChange} />
            <label htmlFor={id}>
                <FileSelectButton selectedFile={selectedFile} />
            </label>
        </span>
    );
}

export default FileSelect;