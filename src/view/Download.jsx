import React, { useEffect, useState } from 'react';
import Link from '@material-ui/core/Link';

export default function Download({ blob }) {
    const [link, setLink] = useState('');

    useEffect(
        () => {
            const url = window.URL.createObjectURL(blob);
            setLink(url);
            return () => window.URL.revokeObjectURL(url);
        },
        [blob, setLink]
    );

    return (
        <Link
            color="primary"
            href={link !== '' ? link : undefined}
            download="result.xml">
        Кликни тук за да свалиш резултатния файл ако той не се свали автоматично!
        </Link>
    );
}