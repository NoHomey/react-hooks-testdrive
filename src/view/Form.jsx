import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Address from './field/Address';
import Bulstat from './field/Bulstat';
import CompanyName from './field/CompanyName';
import ContractNumber from './field/ContractNumber';
import ContractDate from './field/ContractDate';
import DocumentDate from './field/DocumentDate';
import DocumentNumber from './field/DocumentNumber';
import EGN from './field/EGN';
import FirstName from './field/FirstName';
import SecondName from './field/SecondName';
import LastName from './field/LastName';
import FileSelector from './fileSelector/FileSelector';
import SubmitButton from './SubmitButton';

const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Form({
    firstName,
    setFirstName,
    secondName,
    setSecondName,
    lastName,
    setLastName,
    address,
    setAddress,
    bulstat,
    setBulstat,
    companyName,
    setCompanyName,
    contractDate,
    setContractDate,
    contractNumber,
    setContractNumber,
    documentDate,
    setDocumentDate,
    documentNumber,
    setDocumentNumber,
    eGN,
    setEGN,
    file,
    setFile,
    onSubmit
}) {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="md">
        <CssBaseline />
        <div className={classes.paper}>
            <form className={classes.form} noValidate>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={4}>
                        <FirstName firstName={firstName} setFirstName={setFirstName} />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <SecondName secondName={secondName} setSecondName={setSecondName} />
                    </Grid>
                    <Grid item xs={12} sm={4}>
                        <LastName lastName={lastName} setLastName={setLastName} />
                    </Grid>
                    <Grid item xs={12}>
                        <CompanyName companyName={companyName} setCompanyName={setCompanyName} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Bulstat bulstat={bulstat} setBulstat={setBulstat} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <EGN eGN={eGN} setEGN={setEGN} />
                    </Grid>
                    <Grid item xs={12}>
                        <Address address={address} setAddress={setAddress} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <ContractNumber contractNumber={contractNumber} setContractNumber={setContractNumber} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <DocumentNumber documentNumber={documentNumber} setDocumentNumber={setDocumentNumber} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <ContractDate contractDate={contractDate} setContractDate={setContractDate} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <DocumentDate documentDate={documentDate} setDocumentDate={setDocumentDate} />
                    </Grid>
                    <Grid item xs={12}>
                        <FileSelector file={file} onFileChange={setFile} />
                    </Grid>
                </Grid>
                <Grid item container justify='flex-end'>
                    <Grid item>
                        <SubmitButton className={classes.submit} onSubmit={onSubmit} />
                    </Grid>
                </Grid>
            </form>
        </div>
    </Container>
  );
};
