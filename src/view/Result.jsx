import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Download from './Download';

const title = {
    'loading': 'Вашата заявка се обработва',
    'error': 'Възникна грешка',
    'requestSuccess': 'Резултат',
    'requestError': 'Възникна грешка',
    'hide': ''
};

const content = {
    'loading': () => (<CircularProgress />),
    'error': () => (
        <DialogContentText>
            Сървъра е недостъпен. Моля свържете се с администатора.
        </DialogContentText>
    ),
    'requestSuccess': (result) => (
        <DialogContentText>
            <Download blob={result} />
        </DialogContentText>
    ),
    'requestError': (result) => (
        <DialogContentText>
            Възникна грешка. Моля опитайте отново. Ако пак видите това съобщение се свържете с администатора.
        </DialogContentText>
    ),
    'hide': () => null
};

export default function Result({
    result,
    onOk
}) {
    const { state, data } = result;
    const open = state !== 'hide';
    return (
        <Dialog open={open} maxWidth="md" fullWidth>
            <DialogTitle>
                {title[state]}
            </DialogTitle>
            <DialogContent>
                {content[state](data)}
            </DialogContent>
            {state !== 'loading' &&
            <DialogActions>
                <Button onClick={onOk} variant="contained" color="primary">
                    OK
                </Button>
            </DialogActions>}
        </Dialog>
    );
};