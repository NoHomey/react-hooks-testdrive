import { useState, useCallback } from 'react';
import useAPI from './useAPI';

export default function useLogic() {
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [bulstat, setBulstat] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [contractDate, setContractDate] = useState(null);
    const [contractNumber, setContractNumber] = useState('');
    const [documentDate, setDocumentDate] = useState(null);
    const [documentNumber, setDocumentNumber] = useState('');
    const [eGN, setEGN] = useState('');
    const [file, setFile] = useState(null);
    const [result, setData, clear] = useAPI();

    const handleSubmit = useCallback(
        () => setData({
            firstName,
            secondName,
            lastName,
            address,
            bulstat,
            companyName,
            contractDate,
            contractNumber,
            documentDate,
            documentNumber,
            eGN,
            file
        }),
        [firstName, secondName, lastName, address, bulstat, companyName, contractDate, contractNumber, documentDate, documentNumber, eGN, file, setData]
    );

    return [{
        firstName,
        secondName,
        lastName,
        address,
        bulstat,
        companyName,
        contractDate,
        contractNumber,
        documentDate,
        documentNumber,
        eGN,
        file,
        result
    }, {
        setFirstName,
        setSecondName,
        setLastName,
        setAddress,
        setBulstat,
        setCompanyName,
        setContractDate,
        setContractNumber,
        setDocumentDate,
        setDocumentNumber,
        setEGN,
        setFile
    }, {
        handleSubmit,
        clear
    }];
};
