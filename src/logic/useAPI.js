import { useState, useEffect, useCallback } from 'react';
import request from './request';

export default function useAPI() {
    const [result, setResult] = useState({ state: 'hide' });
    const [data, setData] = useState(null);

    const clear = useCallback(
        () => {
            setResult({ state: 'hide' });
        },
        [setResult]
    );
    
    useEffect(
        () => {
            const makeRequest = async () => {
                setResult({ state: 'loading' });
                try {
                    const requestResult = await request(data);
                    setResult({
                        state: requestResult.ok
                            ? 'requestSuccess'
                            : 'requestError',
                        data: requestResult.data
                    });
                } catch(error) {
                    setResult({
                        state: 'error',
                        data: error
                    });
                }
            };

            if(data !== null) {
                makeRequest();
            }
        },
        [data]
    );

    return [result, setData, clear];
};