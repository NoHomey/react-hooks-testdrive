export default function request(data) {
    const formData = new FormData();
    const keys = Object.keys(data);
    for(const key of keys) {
        formData.append(key, data[key]);
    }
    return fetch('api', {
        method: 'POST',
        body: formData
    }).then(res => {
        if(res.ok) {
            return res.blob().then(data => ({
                ok: res.ok,
                data
            }));
        }
        return res.json().then(data => ({
            ok: res.ok,
            data
        }));
    });
};